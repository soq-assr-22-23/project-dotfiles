from store import create_store


def init(store_path):
    """
        Init a dtf-store repository at the given path
    """

    create_store(store_path)

