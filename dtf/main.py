"""
Fichier d'entrée du programme, contient le "main"
"""

import typer
import os
from typing import List, Optional

import commands.init
import commands.git
import helpers

app = typer.Typer(rich_markup_mode="rich")

# État globale partagé dans toutes les commandes
state = { "store-path": "$HOME/.dtf-store" }

@app.command()
def git(command: List[str]):
    """
    Transmet la commande au dépot git contenant les dotfiles

    Exemple:

        dtf git push

    Cette commande pourra être modifié, notamenent pour intégrer une gestion d'erreur. Que ce passe t'il quand le store n'existe pas ? Ou si le store n'est pas un dépot git ?
    """
    store_path = state["store-path"]
    commands.git.git(command, store_path)


@app.command()
def init(clone_remote: str = typer.Option(None, help="Initialise un store de dotfile depuis un dépôt existant.")):
    """
    Initialise un dépot de dotfile si aucun n'est trouvé sur la machine
    Par défaut, le dépot est situé au chemin `$HOME/.config/dtf-store`.

    Le store crée doit être un dépot git.
    """
    store_path = state["store-path"]
    commands.init.init(store_path)


@app.command()
def show():
    """
    Affiche la liste des dotfile disponible.
    """
    commands.show.show()


@app.command()
def install(
        file: str = typer.Option("", help="Installe le seulement fichier"),
        backup: bool = typer.Option(True, help="Crée un backup des fichier avant de les remplacer")
    ):
    """
    Installe les fichiers dotfiles sur la machine courante.
    """
    commands.install.install(file, backup)


@app.callback()
def main(store_path: str = "$HOME/.config/dtf-store"):
    """
    Main callback
    """
    # Extrapolate bash variables to get a value for HOME for instance or ~
    store_path = helpers.extrapolate_bash_variable(store_path)

    # To ease development, use the environment variable DTF-STORE to change the location of the store.
    # So that the dtf-store is located in your dev environment and doesn't pollute your home.
    if os.environ.get("DTF-STORE"):
        state["store-path"] = os.environ["DTF-STORE"]

if __name__ == "__main__":
    app()
