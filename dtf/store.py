import subprocess, os
import tempfile


def create_store(store_path):
    """
    Create a store.

    First, check if the store already exists.
    """
    if os.path.exists(store_path):
        pass
    else:
        os.makedirs(store_path)


