import subprocess, os


def forward_command(command, store_path):
    """
    Forward the git command to the store.
    And print its result to the terminal.
    """
    result = subprocess.check_output(["git"] + command, cwd=store_path)
    print(result.decode())

