{ pkgs, ... }:

{
  # https://devenv.sh/basics/
  env.GREET = "devenv";

  # https://devenv.sh/packages/
  packages = [ pkgs.git pkgs.poetry ];

  languages.python = {
  enable = true;
    package =  pkgs.python3.withPackages (p: with p; [
      typer
      pytest
      pyfakefs
      pytest-cov
      dateutil
    ]);
  };

  enterShell = ''
    zsh; exit
  '';

}
