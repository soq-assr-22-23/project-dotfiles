# Projet qualité logiciel - Gestionnaire de dotfiles

## Modalités et Rendu

- À faire en binôme (si vous voulez faire des trinômes les **bonus** deviennent obligatoires :smiling_imp:, et personne ne doit se retrouver seul).
- Il est demandé un rapport très léger en format PDF qui ne peut en aucun cas être le README. 
  Le rapport devra contenir:
  - Une explication de votre projet: ce que vous avez fait et comment.
  - Ainsi que les réponses aux questions ouvertes disséminées dans ce document.
  - Toute explication ou élément de réponse que vous jugerez utile.
- Le dépôt du projet fait intégralement partie du rendu final, donc soignez le !

## Objectif(s)

L'objectif de ce TP est de mettre en place un système de gestion pour vos `dotfiles` tout en respectant les différents principes de qualité logiciel vus en cours; ainsi que de pratiquer l'utilisation de git en situation de développement en binôme.

## Description

Les `dotfiles` désignent l'ensemble des fichiers de configuration des divers logiciels que vous utilisez: git, vim, emacs etc.
Maintenir un dépôt de `dotfiles` est souvent utile pour faciliter la migration de ses outils entre différentes machines.

L'objectif de ce TP est de mettre en place un système de gestion de `dotfiles`.
En utilisant la base de code fourni, vous aller devoir développer un projet de gestion de `dotfiles` en python.

## Fonctionnalités

Le projet comportera les fonctionnalités suivantes:

**Gestion d'un dépôt git local contenant les `dotfiles`**

L'utilisation de git permet de garder un historique des modifications apporté à vos  `dotfiles`, ainsi que de permettre le stockage sur un serveur git (gitlab, github etc).
Les fichiers `dotfiles` seront donc conservés dans un dépôt git, indépendant du dépôt de ce projet, situé dans le `home` de l'utilisateur. On appellera ce dépôt le `store`.
Pour manipuler le dépôt de `dotfiles`, le projet python permettra de transmettre les commandes git au dépôt de `dotfiles` situé dans le `home`. Cela facilitera l'utilisation par les utilisateurs qui n'auront pas à se déplacer manuellement dans le dossier de `dotfiles`.

Voici un exemple de transmissions de commandes git:

```bash
# Mise-à-jours du dépôt
dtf git pull

# Sauvegarde des modifications
dtf git commit -m "vimrc: add shortcut for chatgpt"
```

Les actions suivantes devront être réalisées pour garantir le fonctionnement du programme:
- Création et initialisation d'un dépôt git sur la machine en cas de première utilisation. Ce sera le nouveau store à utiliser.
  Exemple: `dtf init`
- Le store pourra également être cloné depuis un dépôt git pour importer vos `dotfiles` d'une machine à l'autre.  Exemple: `dtf init clone https://github.com/bob/my-dotfiles`.

**Installation sur la machine des dotfiles**

Le projet doit permettre l'installation des `dotfiles` sur la machine courante.
Pour installer les dotfiles, les différents fichiers de configuration devront être positionnés aux différents endroits attendus par les utilisateurs (par exemple `~/.vimrc` pour vim, `~/.config/git/config` pour git etc).

Pour installer un fichier, votre programme pourra utiliser plusieurs stratégies.

- Installation en lien symbolique (conseillé et par défaut).
  Dans ce mode, les fichiers seront installé en créant un [lien symbolique](https://fr.wikipedia.org/wiki/Lien_symbolique) sur l'ordinateur pointant vers le fichier en question dans le dépôt de `dotfiles`.
- Installation avec copie directe. Dans ce mode, les fichiers seront directement copiés sur le système supprimant les anciens fichiers.

Pour chacun de ces modes, une option pour sauvegarder des anciens fichiers doit être disponible.
(Attention, comment sauvegarder un fichier qui est déjà un lien symbolique? Est-il nécessaire de sauvegarder un lien pointant déjà sur votre dossier de `dotfiles`? Pourquoi ?)

**Installation d'un seul fichier**

Il doit être possible d'installer un seul fichier. Créer une option le permettant dans la ligne de commande.

**Association d'un fichier à un emplacement**

Le projet `dtf` ne peut pas connaître à l'avance ou se situent chacun des fichiers, pour palier à ce comportement, le dossier de `dotfiles` contiendra également un fichier d'association à sa racine.
Cela peut être un fichier `json`, `yaml` ou `toml`.

Dans la version simple, l'utilisateur devra gérer ses `dotfiles` à la main, ainsi que mettre à jour le fichier d'association.
Le point suivant (en bonus) permet d'automatiser certaines étapes.
**Notez également, que la documentation devra expliquer clairement comment gérer le fichier d'association.**

Exemple de fichier d'association avec un seul `dotfile`.

```yaml
- installation-path: "$HOME/.vimrc" # Chemin du fichier une fois installé sur la machine
  file: "my-vimrc" # Nom du fichier dans votre repos dotfiles
```

*Ne perdez pas de temps à implémenter votre parser de `json`, `yaml` ou `toml`, Utilisez une bibliothèque*.

**Commande pour ajouter/supprimer des `dotfiles` (bonus)**

Ajouter une commande à votre programme pour permettre l'ajout (et la suppression) de fichier `dotfile`  dans le fichier d'association.

Par exemple:

```bash
# dtf dot-add <lien vers un fichier> <chemin d'installation>
dtf code/vimrc $HOME/.vimrc
```

Dans le cas de la commande ci-dessus, le fichier dans le dossier `code/vimrc` est ajouté au store de `dotfiles` et le fichier d'association est mis à jour.
Vous pouvez également créer un `commit` git automatiquement pour simplifier la gestion des `dotfiles` à l'utilisateur.

**Restauration d'un backup (bonus)**

Depuis un dossier de backup, permettre de restaurer les fichiers d'origines.

## Qualité logiciel

**Qualité logiciel**

Les critères de qualité logiciel vue en cours doivent être appliqué dans ce projet. Éviter la duplication de code, séparer les différentes préoccupations, garder le code simple si possible etc...

De plus voici une liste de critères à respecter:
- Les fonctions seront toutes commentées avec un [docstring](https://peps.python.org/pep-0257/) python, expliquant ce qu’elles font, leurs entrées et leur sortie (si il y en a).
- Les parties du code difficiles devront être agrémentées d'un commentaire d'explication.
- Bien séparer les parties du code indépendantes en différentes fonctions, faire un module si nécessaire.

**Utilisation de git**

Le projet étant réalisé en binôme, vous allez devoir utiliser le logiciel de gestion git pour gérer votre développement.
Il n'y a pas de workflow imposé, ce sera à vous de choisir. Mais j'attends, dans le rapport final une explication de votre workflow ainsi que des éléments de critiques qui vous ont influencé vers ce choix.
De plus, à la fin du projet j'attends une analyse de votre workflow: ce qui a fonctionné, ce qui n'a pas fonctionné, pourquoi ? Ainsi que des pistes d'améliorations si vous en avez.
Une première piste à suivre pour choisir votre workflow est ce guide: https://www.atlassian.com/fr/git/tutorials/comparing-workflows.

**ATTENTION**: Pour ce projet, il est nécessaire de distinguer le dépôt `git` de code, et le dépôt `git` contenant les `dotfiles` (AKA le store).

**Test de code**

Le code devra être testé afin d'atteindre une bonne couverture de code (>90% si possible). Il doit être possible de lancer les tests simplement avec une ligne de commande.

**Packaging**

Comme vu dans le TP [pytest](https://adfaure.github.io/files/teaching/2022-software-quality/4-pytest.pdf), mettre en place un packaging de votre programme.
Trouver comment indiquer dans votre paquet que votre programme contient un exécutable.

**Documentation**

Votre projet devra contenir une documentation (au moins un README.md) contenant les informations suivantes:

- Pour les utilisateurs:
    - Explication haut-niveau du projet pour les utilisateurs. Pourquoi devraient ils utiliser votre projet, à quoi ça sert etc...
    - Comment installer le projet ?
    - Comment se servir des commandes ?
- Pour les développeurs:
    - Comment installer les outils de développement ?
    - Comment exécuter les tests ?
    - Quel est le workflow git ?

**Intégration Continue (CI) (bonus)**

Utiliser le système d'intégration continue de gitlab pour exécuter vos tests automatiquement lors d'un `push`  sur votre dépôt gitlab.

**Documentation (bonus)**

Des outils plus avancés (que le README) pour la documentation sont disponibles, par exemple [sphinx](https://www.sphinx-doc.org/en/master/), ou [mdbook](https://rust-lang.github.io/mdBook/).
Utiliser un de ses outils pour construire votre documentation, et la mettre à disposition via [gitlabpages](https://docs.gitlab.com/ee/user/project/pages/).

Un README sera toujours **requis**. Cependant, celui-ci pourra être un peu plus léger à condition qu’il fournisse un lien vers la documentation.

*bonus++*: Il est également possible d'automatiser la construction de la documentation et le déploiement grâce au CI gitlab.

**Outils de développement (bonus)**

Certains outils sont disponibles pour vous aider à maintenir une base de code propre.
Voici une liste (non exhaustive) de ces outils:

* [black](https://black.readthedocs.io/en/stable/)
* [flake8](https://flake8.pycqa.org/en/latest/)
* [isort](https://pycqa.github.io/isort/)
* [poetry](https://python-poetry.org/) (pour le packaging).

Vous pouvez mettre en place ces outils (ou une sous partie), expliquer vos choix, et la façon dont ils s'inscrivent dans votre workflow de développement.

## Mise en place du projet

**Création du dépôt**

Utiliser votre compte [gitlab](https://gitlab.com) pour forker le [dépôt](https://gitlab.com/soq-assr-22-23/project-dotfiles) du projet.

**Attention:** ne faire qu’un seul fork par groupe et travailler bien sur ce fork ou votre travail ne sera pas pris en compte (demandez moi de valider une fois la manipulation effectuée).

Si c'est bien un fork, mes commits de mise en place doivent apparaître dans votre historique.

**Installation des outils**

Le langage du projet imposé est le python, il existe plusieurs méthodes pour gérer l'environnent logiciel pour développer.
Vous pouvez utilisez [`virtual environment`](https://docs.python.org/fr/3/library/venv.html), ou [poetry](https://python-poetry.org/) qui permet également de gérer les versions des dépendances.
(Personnellement, j'utilise [`devenv`](https://github.com/cachix/devenv) qui est encore un projet expérimental basé sur le gestionnaire de paquet [nix](https://nixos.org).)

Voici la liste des dépendances du projet:

- python 3
- [pytest](https://docs.pytest.org/en/7.2.x/)
- pytest-cov
- [typer](https://typer.tiangolo.com/)
- [pyfakefs](https://pypi.org/project/pyfakefs/)

*Libre à vous d'ajouter des dépendances si nécessaire, mais j'attends que vos choix soient justifiés dans votre rapport*.

**Utilisation du projet**

Pour utiliser le projet pendant le développement:
- `python dtf/main.py` pour lancer le programme.
- `pytest` pour lancer les tests.

**Quelques points technique**

- Une des difficultés du projet est la mise en place des tests. En effet, le projet crée un programme qui impacte directement votre système de fichiers.
  Hors, vous n'avez probablement pas envie que lorsque vous développez une fonctionnalité pour `dtf` il y ait des effets de bords sur votre système de fichiers.
  - Pour cela, vous pouvez utiliser la bibliothèque de tests `pyfakefs`. Cependant, cette bibliothèque présente des limitations, notamment dans l'utilisation des sous-processus linux.
  - Une autre solution est l'utilisation de fichiers temporaires, une exemple est disponible dans `tests/test_store.py`.
  - Il est également possible de mettre en place des tests via docker si vous êtes à l'aise.

- J'ai fait le choix d'utiliser la bibliothèque de ligne de commande `typer`, à vous de la prendre en main pour compléter le projet. Ne pas hésiter à lire la documentation pour comprendre les détails de son utilisation.

## Références et documentation utile

Pour savoir si un dossier est un dépôt git: https://remarkablemark.org/blog/2020/06/05/check-git-repository/. Cela est utile pour vérifier si le store est bien un dépôt git.
